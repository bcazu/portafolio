export default {
  methods: {
    inlineBgImage(src) {
      let bgImage = require("@/assets/img/" + src);
      return {
        backgroundImage: `url("${bgImage}")`
      };
    }
  }
}
