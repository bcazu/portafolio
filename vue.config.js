const path = require('path');
module.exports = {
    configureWebpack: {
        devtool: 'source-map',
        resolve: {
            alias: {
                '@': path.resolve(__dirname, 'src'),
                'components': path.resolve(__dirname, 'src/components'),
            }
        }
    },
    productionSourceMap: true
}